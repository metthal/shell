/**
 * @file proj2.c
 * @author Marek Milkovič (xmilko01)
 * @brief This project was created for course Advanced Operating Systems at FIT BUT 2015/2016.
 */
#define _XOPEN_SOURCE 700

#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define UNUSED(x)                   (void)x
#define DEFAULT_READ_BUFFER_SIZE    513
#define SHELL_PROMPT                "$ "

/**
 * Enums for parser state machine.
 */
enum command_parser_state
{
	STATE_PROGRAM, ///< State for parsing name of the program.
	STATE_ARGS, ///< State for parsing the arguments of the program.
	STATE_TRAILER ///< State for parsing the additional information at the end such as &,<,>
};

/**
 * Represents single command that is entered into shell after pressing Enter key.
 */
struct command
{
	char* program; ///< Program to run.
	uint32_t args_count; ///< Number of arguments to the program.
	char** args; ///< Arguments.
	char* input; ///< Redirected input. If NULL, then stdin.
	char* output; ///< Redirected output. If NULL, then stdout.
	uint8_t bg; ///< Indicates whether to run command in the background or foreground.
};

/**
 * Job manager shared across threads and processes. Provides management of the currently active
 * command, currently running job (process) and synchronization mechanisms for synchronization with shell.
 */
struct job_manager
{
	struct command* command; ///< Currently executed command.

	pthread_cond_t ready; ///< Condition variable indicating whether command is ready to be executed.
	pthread_cond_t finished; ///< Condition variable indicating whether job finished execution.
	pthread_mutex_t lock; ///< Mutex that needs to be locked (if needed) when manipulating with job manager.

	pid_t fg_job_pid; ///< PID of currently executed job in the foreground.

	int stop_jobs; ///< If this value is 1, then some external source (user typing in the shell) wants to quit.
};

struct job_manager* job_mgr = NULL; ///< Global job manager shared across threads and processes.

/**
 * Signal handler for SIGCHLD signal. It terminates all hanging child processes.
 * Every background process reports its termination to the standard output. Foreground
 * process resets PID in job manager.
 *
 * @param sig Signal received.
 */
void handle_sigchild(int sig)
{
	UNUSED(sig);

	pid_t child_pid;

	// waitpid() needs to be called for every terminated children, otherwise they hang as zombies
	while ((child_pid = waitpid(-1, NULL, WNOHANG)) > 0)
	{
		if (child_pid != job_mgr->fg_job_pid)
			printf("Background process %u done.\n", child_pid);
		else
			job_mgr->fg_job_pid = 0;
	}
}

/**
 * Signal handler for SIGINT signal. Passes interrupt to the running foreground background
 * if any is running.
 *
 * @param sig Signal received.
 */
void handle_sigint(int sig)
{
	UNUSED(sig);

	if (job_mgr->fg_job_pid != 0)
		kill(job_mgr->fg_job_pid, SIGINT);
}

/**
 * Performs selected operation of sigprocmask() on the variadic list of signals.
 *
 * @param op Operation of sigprocmask().
 * @param count Number of signals.
 * @param args Variadic list of signals.
 *
 * @return 0 if success, otherwise error.
 */
int op_signals(int op, uint32_t count, va_list* args)
{
	sigset_t signal_set;
	if (sigemptyset(&signal_set) != 0)
		return -1;

	for (uint32_t i = 0; i < count; ++i)
	{
		int sig = va_arg(*args, int);
		if (sigaddset(&signal_set, sig) != 0)
			return -1;
	}

	return sigprocmask(op, &signal_set, NULL);
}

/**
 * Blocks the specified signals.
 *
 * @param count Number of signals.
 *
 * @return 0 if success, otherwise error.
 */
int block_signals(uint32_t count, ...)
{
	va_list args;
	va_start(args, count);

	int ret = op_signals(SIG_BLOCK, count, &args);

	va_end(args);
	return ret;
}

/**
 * Unblocks the specified signals.
 *
 * @param count Number of signals.
 *
 * @return 0 if success, otherwise error.
 */
int unblock_signals(uint32_t count, ...)
{
	va_list args;
	va_start(args, count);

	int ret = op_signals(SIG_UNBLOCK, count, &args);

	va_end(args);
	return ret;
}

/**
 * Waits until some signal arrives and changes value of the given variable to expected value.
 *
 * @param value Value to check.
 * @param expected Expected value.
 *
 * @return 0 if success, otherwise error.
 */
int wait_for(int* value, int expected, int signal)
{
	if (block_signals(1, signal) != 0)
		return -1;

	sigset_t emptyset;
	if (sigemptyset(&emptyset) != 0)
		return -1;

	while (*value != expected)
		sigsuspend(&emptyset);

	return unblock_signals(1, signal);
}

/**
 * Initialize all signal handlers.
 *
 * @return 0 if success, otherwise error.
 */
int init_signal_handlers()
{
	if (block_signals(2, SIGCHLD, SIGINT) != 0)
		return -1;

	// SIGCHLD handler
	struct sigaction chld_action;
	chld_action.sa_handler = &handle_sigchild;
	sigemptyset(&chld_action.sa_mask);
	chld_action.sa_flags = 0;

	// SIGINT handler
	struct sigaction int_action;
	int_action.sa_handler = &handle_sigint;
	sigemptyset(&int_action.sa_mask);
	int_action.sa_flags = 0;

	if (sigaction(SIGCHLD, &chld_action, NULL) != 0)
		return -1;

	if (sigaction(SIGINT, &int_action, NULL) != 0)
		return -1;

	return unblock_signals(2, SIGCHLD, SIGINT);
}

/**
 * Initializes job manager.
 */
void init_job_manager()
{
	job_mgr = (struct job_manager*)malloc(sizeof(struct job_manager));
	job_mgr->command = NULL;
	pthread_cond_init(&job_mgr->ready, NULL);
	pthread_cond_init(&job_mgr->finished, NULL);
	pthread_mutex_init(&job_mgr->lock, NULL);
	job_mgr->fg_job_pid = 0;
	job_mgr->stop_jobs = 0;
}

/**
 * Releases any memory owned by job manager.
 */
void free_job_manager()
{
	pthread_cond_destroy(&job_mgr->ready);
	pthread_cond_destroy(&job_mgr->finished);
	pthread_mutex_destroy(&job_mgr->lock);
	free(job_mgr);
}

/**
 * Parses argument from the given string. Argument is separated by whitespaces, which can be in any
 * amount in front or at the end of the argument and are stripped away from the argument.
 *
 * @param arg_str String with argument in it. It is set to point behind the argument after function finishes.
 *
 * @return Argument string.
 */
char* parse_arg(char** arg_str)
{
	char* itr = *arg_str;

	// Skip initial whitespaces.
	while (isspace(*itr))
		itr++;

	// Any of these symbols indicates beginning of the trailing section of the command.
	if (*itr == '&' || *itr == '>' || *itr == '<')
	{
		*arg_str = itr;
		return NULL;
	}

	// Go through characters of the argument.
	char* arg_start = itr;
	while (!isspace(*itr) && *itr != '\0')
		itr++;

	// Calculate length of the argument (without trailing and prefix whitespaces).
	size_t arg_len = itr - arg_start;
	if (arg_len == 0)
	{
		*arg_str = itr;
		return NULL;
	}

	// Skip trailing whitespaces.
	while (isspace(*itr))
		itr++;

	// Allocate memory for argument and copy it.
	char* arg = (char*)calloc(arg_len + 1, 1);
	memcpy(arg, arg_start, arg_len);

	// Set arg_str to point behind the current argument.
	*arg_str = itr;
	return arg;
}

/**
 * Parses the arguments from the given string. The description of argument is given above the function
 * @ref parse_arg.
 *
 * @param args_str String containing arguments.
 * @param args_count Output parameter that contains the amount of parsed arguments.
 *
 * @return Arguments.
 */
char** parse_args(char** args_str, uint32_t* args_count)
{
	char* itr = *args_str;

	uint32_t allocated_args = 0;
	char** args = NULL;
	*args_count = 0;

	// Parse arguments until we can
	char* arg;
	while ((arg = parse_arg(&itr)) != NULL)
	{
		// Since we don't know the amount of arguments when we are parsing,
		//   we need to reallocate more and more space as we go. We start with
		//   space for 16 arguments and double it every other time we need reallocation.
		//   If there are no arguments, this code won't even be run and arguments won't
		//   allocate anything.
		if (allocated_args == 0 || *args_count == allocated_args)
		{
			allocated_args = allocated_args == 0 ? 16 : allocated_args << 1;
			args = (char**)realloc(args, allocated_args * sizeof(char*));
		}

		args[*args_count] = arg;
		(*args_count)++;
	}

	*args_str = itr;
	return args;
}

/**
 * Parses the trailing part of the command. Trailing part contains information like background run (&),
 * redirected input source (<) or redirected output destination (>).
 *
 * @param trailer_str String that contains trailer.
 * @param input Output parameter, where to store redirected input file name. NULL otherwise.
 * @param output Output parameter, where to store redirected output file name. NULL otherwise.
 * @param bg Output parameter, where to store 1 if it is background run or 0 if it is foreground run.
 */
void parse_trailer(char** trailer_str, char** input, char** output, uint8_t* bg)
{
	char* itr = *trailer_str;

	*input = NULL;
	*output = NULL;
	*bg = 0;

	while (*itr != '\0')
	{
		switch (*itr)
		{
			// Redirected input
			case '<':
				itr++;
				*input = parse_arg(&itr);
				break;
			// Redirected output
			case '>':
				itr++;
				*output = parse_arg(&itr);
				break;
			// Background run
			case '&':
				itr++;
				*bg = 1;
				break;
		}

		// Skip rest of whitespaces
		while (isspace(*itr))
			itr++;
	}

	*trailer_str = itr;
}

/**
 * Parses the command from the input string.
 *
 * @param command_str String with command.
 *
 * @return Parse command, or NULL in case of error.
 */
struct command* parse_command(char* command_str)
{
	// Initializer parser state machine.
	uint8_t parser_state = STATE_PROGRAM;
	char* itr = command_str;

	struct command command;
	memset(&command, 0, sizeof(struct command));

	while (*itr != '\0')
	{
		switch (parser_state)
		{
			// First we parse program as one argument.
			case STATE_PROGRAM:
				command.program = parse_arg(&itr);
				parser_state = STATE_ARGS;
				break;
			// Then, we parse arguments to our program.
			case STATE_ARGS:
				command.args = parse_args(&itr, &command.args_count);
				parser_state = STATE_TRAILER;
				break;
			// In the end, we parse additional information about the run from the trailer.
			case STATE_TRAILER:
				parse_trailer(&itr, &command.input, &command.output, &command.bg);
				break;
			default:
				return NULL;
		}
	}

	// Allocate memory because different thread is going to use this object, so we cannot keep it
	//   as stack object.
	struct command* ret_command = (struct command*)malloc(sizeof(struct command));
	memcpy(ret_command, &command, sizeof(struct command));
	return ret_command;
}

/**
 * Reads the command from the standard input separated by Enter keypress. Prints prompt '$' for
 * before waiting for input.
 *
 * @param stop Set to 1 if input ended with EOF.
 *
 * @return Parsed command.
 */
struct command* read_command(uint32_t* stop)
{
	char read_buffer[DEFAULT_READ_BUFFER_SIZE];

	// Print prompt '$'
	printf("%s", SHELL_PROMPT);
	fflush(stdout);

	size_t char_count = 0;
	int read_char;

	// Read until we hit new line (Enter key) or we read 513th character
	while (((read_char = getchar()) != '\n') &&
			(char_count < DEFAULT_READ_BUFFER_SIZE))
	{
		if (read_char == EOF)
		{
			*stop = 1;
			return NULL;
		}

		read_buffer[char_count++] = read_char;
	}

	// Clear out input buffer until newline if we ran into long input.
	if (char_count == DEFAULT_READ_BUFFER_SIZE)
	{
		while (getchar() != '\n');
		fputs("Input too long", stderr);
		return NULL;
	}

	// Null terminate read string, so it can be processed using common string functions.
	read_buffer[char_count] = '\0';

	return parse_command(read_buffer);
}

/**
 * Releases all memory owned by command object and also command itself.
 *
 * @param command Command to release from memory.
 */
void free_command(struct command* command)
{
	free(command->program);
	free(command->input);
	free(command->output);

	if (command->args)
	{
		for (size_t i = 0; i < command->args_count; ++i)
			free(command->args[i]);
	}

	free(command);
}

/**
 * Handles the command behavior. If the commands program name is 'exit', it is handled
 * like request to exit shell and stop variable is set. Otherwise, ready condition variable
 * of job_mgr is used to indicate that command is prepared for execution and handler
 * waits for finished condition variable to be signaled.
 *
 * @param command Command to handle.
 * @param stop Set to 1 if command is 'exit'.
 */
void handle_command(struct command* command, uint32_t* stop)
{
	if (command->program == NULL)
	{
		free_command(command);
		return;
	}

	if (strcmp(command->program, "exit") == 0)
	{
		*stop = 1;
		free_command(command);
		return;
	}

	pthread_mutex_lock(&job_mgr->lock);
	job_mgr->command = command;
	pthread_cond_signal(&job_mgr->ready);
	pthread_mutex_unlock(&job_mgr->lock);

	pthread_mutex_lock(&job_mgr->lock);
	while (job_mgr->command != NULL)
		pthread_cond_wait(&job_mgr->finished, &job_mgr->lock);
	pthread_mutex_unlock(&job_mgr->lock);

	free_command(command);
}

/**
 * Runs the command as new process with its arguments and input/output source. This function
 * returns only in case of error (requested executable program was not found). Otherwise, the process
 * from which this function is called is replaced by entirely new process.
 *
 * @param cmd Command to execute.
 */
void run_command(struct command* cmd)
{
	// We need to completely reallocate arguments of the command as there are different requirements.
	// execvp() requires argv to contain program name at argv[0] and also to be null-terminated array.
	char** argv = (char**)malloc((cmd->args_count + 2) * sizeof(char*));

	// Copy program name.
	argv[0] = (char*)malloc(strlen(cmd->program) + 1);
	strcpy(argv[0], cmd->program);

	// Copy arguments.
	for (size_t i = 0; i < cmd->args_count; ++i)
	{
		argv[i + 1] = (char*)malloc(strlen(cmd->args[i]) + 1);
		strcpy(argv[i + 1], cmd->args[i]);
	}

	// Null terminate array.
	argv[cmd->args_count + 1] = NULL;

	// If command has redirected output.
	if (cmd->output)
	{
		// Try to open it for writing. If it does not exist, create it with 0644 rights.
		int output_fd = open(cmd->output, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

		// The file could not be opened or created - just use stdout and print error.
		if (output_fd == -1)
			perror(cmd->output);
		else
		{
			// Close regular stdout and duplicate the file descriptor into the descriptor of stdout.
			close(STDOUT_FILENO);
			dup2(output_fd, STDOUT_FILENO);
		}
	}

	// If command has redirected input.
	if (cmd->input)
	{
		// Try to open it for reading.
		int input_fd = open(cmd->input, O_RDONLY);

		// The file could not be opened - just use stdin and print error.
		if (input_fd == -1)
			perror(cmd->input);
		else
		{
			// Close regular stdin and duplicate the file descriptor into the descriptor of stdin.
			close(STDIN_FILENO);
			dup2(input_fd, STDIN_FILENO);
		}
	}

	// Try to run the program and search for it in PATH.
	if (execvp(cmd->program, argv) != 0)
	{
		for (size_t i = 0; i < cmd->args_count + 1; ++i)
			free(argv[i]);
		free(argv);
		free_command(cmd);
		free_job_manager();

		perror(cmd->program);
	}
}

/**
 * Entry routine for command that waits on ready condition variable of job manager and run commands.
 *
 * @param arg The argument into the entry routine. Unused right now.
 *
 * @return The return value of the routine. Unused right now.
 */
void* run_commands_thread(void* arg)
{
	UNUSED(arg);

	while (1)
	{
		// Wait until there is some command to run.
		pthread_mutex_lock(&job_mgr->lock);
		while (!job_mgr->stop_jobs && job_mgr->command == NULL)
			pthread_cond_wait(&job_mgr->ready, &job_mgr->lock);

		// This is spurious wake-up. Someone just wants to exit and woke us up. Exit thread.
		if (job_mgr->stop_jobs)
		{
			pthread_mutex_unlock(&job_mgr->lock);
			pthread_exit(NULL);
		}

		struct command* command = job_mgr->command;
		pthread_mutex_unlock(&job_mgr->lock);

		// We need to start blocking before fork(), because we need to initialize
		//   job_mgr with info available after fork(), but at the moment we can already
		//   receive SIGCHLD. We don't have to block in case of background run, because
		//   just fg_job_pid is set and that is not related to background jobs.
		if (!command->bg)
			block_signals(1, SIGCHLD);

		pid_t process_pid = fork();
		if (process_pid == 0)
		{
			// Child, execute program
			run_command(command);
			exit(1);
		}
		else
		{
			// This is executed just by parent process.
			if (!command->bg)
			{
				// Set current job pid to be child PID.
				job_mgr->fg_job_pid = process_pid;

				// Now we can safely unblock signals, because fg_job_pid is set to proper value.
				unblock_signals(1, SIGCHLD);

				// Wait until fg_job_pid is set to 0.
				// That happens in SIGCHLD handler.
				wait_for(&job_mgr->fg_job_pid, 0, SIGCHLD);
			}

			// Signal job manager that the current job is finished.
			pthread_mutex_lock(&job_mgr->lock);
			job_mgr->command = NULL;
			pthread_cond_signal(&job_mgr->finished);
			pthread_mutex_unlock(&job_mgr->lock);
		}
	}

	return NULL;
}

int main()
{
	init_signal_handlers();
	init_job_manager();

	pthread_t runner_thread;
	pthread_create(&runner_thread, NULL, &run_commands_thread, job_mgr);

	struct command* cmd;
	uint32_t stop = 0;
	while (!stop)
	{
		cmd = read_command(&stop);
		if (cmd == NULL)
			continue;

		handle_command(cmd, &stop);
	}

	pthread_mutex_lock(&job_mgr->lock);
	job_mgr->stop_jobs = 1;
	pthread_cond_signal(&job_mgr->ready);
	pthread_mutex_unlock(&job_mgr->lock);

	pthread_join(runner_thread, NULL);

	free_job_manager();
	return 0;
}
